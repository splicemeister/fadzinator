﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fadz.Randomizer
{
    ///<summary>
    ///Random : Generator Class
    ///</summary>
    public class RandomGenerator
    {
        /// <summary>
        /// Simple random number generator
        /// </summary>
        /// <param name="low">Lowest limit(optional, default is 1 if not filled)</param>
        /// <param name="high">Highest limit minus one(optional, default is 11 if not filled.
        /// Which is equivalent to 10 because of minus one)</param>
        public int GenerateSimple(int low = 1, int high = 11)
        {
            var result = 0;
            var random = new Random();
            result = random.Next(low, high);
            return result;
        }

        /// <summary>
        /// x count simple random numbers generator
        /// </summary>
        /// <param name="count">The number of random numbers you wanted to generate.
        /// (optional, default is 10 if not filled)</param>
        /// <param name="low">Lowest limit(optional, default is 1 if not filled)</param>
        /// <param name="high">Highest limit minus one(optional, default is 11 if not filled.
        /// Which is equivalent to 10 because of minus one)</param>
        /// <returns name="List of int">List of random numbers depends on the count that you set.
        /// (optional, default is 10 if not filled)</returns>
        public List<int> GenerateSimples(int count = 10, int low = 1, int high = 11)
        {
            //List is not a good choice here if you are trying to generate thousands of millions random #.
            //You might want to change it to a faster collection such as HashSet or Dictionary
            var result = new List<int>();
            var random = new Random();
            for (int i = 1; i <= count; i++)
            {
                result.Add(random.Next(low, high));
            }
            return result;
        }

        /// <summary>
        /// x count Unique random numbers generator
        /// </summary>
        /// <param name="count">The number of random numbers you wanted to generate.
        /// (optional, default is 10 if not filled)</param>
        /// <param name="low">Lowest limit(optional, default is 1 if not filled)</param>
        /// <param name="high">Highest limit minus one(optional, default is 11 if not filled.
        /// Which is equivalent to 10 because of minus one)</param>
        /// <returns name="List of int">List of random numbers depends on the count that you set.
        /// (optional, default is 10 if not filled)</returns>
        /// <remarks name="Warning">Make sure that the High parameter is higher than count.</remarks> 
        public List<int> GenerateUniqueRandoms(int count = 10, int low = 1, int high = 10)
        {
            //List is not a good choice here if you are trying to generate thousands of millions random #.
            //You might want to change it to a faster collection such as HashSet or Dictionary
            var result = new List<int>();
            var random = new Random();
            //We will use while loop in this case until the result is filled.
            while (result.Count != 10)
            {
                var generated = random.Next(low, high);
                if (!result.Contains(generated))
                    result.Add(generated);
            }
            return result;
        }
    }
}
