﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fadz.Randomizer
{
    ///<summary>
    ///Random : Main entry point
    ///</summary>
    public class Program
    {
        public static void Main(string[] args)
        {
            var randomizer = new RandomGenerator();

            //Display 1 simple random #
            Console.Write("Display 1 simple random # ");
            Console.WriteLine(randomizer.GenerateSimple(0, 10));

            //Display 10 simple random #
            string tenSampleRandomNumbers = "";
            foreach (int i in randomizer.GenerateSimples(10, 1, 11))
            {
                tenSampleRandomNumbers += i + ", ";
            }
            Console.Write("Display 10 simple random # ");
            Console.WriteLine(tenSampleRandomNumbers);

            //Display 10 uniques random #
            string tenUniqueRandomNumbers = "";
            foreach (int i in randomizer.GenerateUniqueRandoms(10, 1, 11))
            {
                tenUniqueRandomNumbers += i + ", ";
            }
            Console.Write("Display 10 uniques random # ");
            Console.WriteLine(tenUniqueRandomNumbers);

            Console.ReadLine(); //Let the console stay for viewing purpose;
        }
    }
}
