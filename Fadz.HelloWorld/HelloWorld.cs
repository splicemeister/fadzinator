﻿using System;

namespace Fadz.HelloWorld
{
    ///<summary>
    ///Hello World in c#
    ///</summary>
    public class HelloWorld
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello, world!");
            Console.ReadLine();
        }
    }
}
